# Comments

Project based on instructions of [README.md](README.md)

## Architecture

Consist of MVC with communication layers:

- View
- Controller
- Manager
- Business
- Provider

`Views` do not know about `Controllers` and can only communicate with `Controllers`, if needed, using protocols
`Controllers` can communicate with `Views`, other `Controllers` and `Managers`
`Managers` can communicate with `Business` and, on some rare ocasions, other `Managers`
`Business` communicate with other `Business` and with `Providers`

Also their are two auxiliar layers that are common to all layers:

- Model
- Infrastructure

This architecture was chosen to securely separate each of the layers of the app and avoid any unecessary access between layers. It also keeps organized with responsabilities of each layer.

## Third Party

Libraries were chosen to make the programming easier given the amount of time to develop:

- SwiftLint: for swift programming best practice reminder
- Alamofire: to avoid having to create everything related to Networking, it's an easy to use Framework
- SwiftyJSON: also to make a little bit easier when working with dictionary data. On the first commits I was using new decode options of Swift 4, but ended up switching for SwiftyJSON when I realized I had to make two models instead of only the Core Data one

## Future improvements

There's a lot I'd like to work on:

- Finish implementing UITests - Only started these to test `Views` and `Controllers`, even though there's already a good head start.
- Implement Unit Tests for `Business` layer, mocking all providers
- Implement `Error` scenarios
- Start presenting saved locations on start view, instead of empty data
- Possibility to **Save**/**Delete** locations even when all locations are presented on map
- Implement live search logic
- Implement Unit Tests for `Controller` layer. It's something that I haven't implemented yet, but have started on this project
- Improve comments on code