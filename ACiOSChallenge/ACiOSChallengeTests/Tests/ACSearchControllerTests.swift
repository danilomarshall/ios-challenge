//
//  ACSearchControllerTests.swift
//  ACiOSChallengeTests
//
//  Created by Danilo S Marshall on 27/07/18.
//  Copyright © 2018 DevTest. All rights reserved.
//

import XCTest
@testable import ACiOSChallenge

struct TestDisplaySaveOrderFailure {
    static var presentViewControllerAnimatedCompletionCalled = false
    static var viewControllerToPresent: UIViewController?
}

extension ACSearchViewController {
    override open func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        TestDisplaySaveOrderFailure.presentViewControllerAnimatedCompletionCalled = true
        TestDisplaySaveOrderFailure.viewControllerToPresent = segue.destination
    }
}

class ACSearchControllerTests: XCTestCase {
    
    // MARK: - Constants
    
    private let kControllerStoryboard = "Main"
    private let kControllerIdentifier = "ACSearchViewController"
    
    // MARK: - Properties
    
    private var controller: ACSearchViewController?
    private var window: UIWindow?
    
    // MARK: - Overrides
    
    override func setUp() {
        super.setUp()
        window = UIWindow()
        setupACSearchViewController()
    }
    
    override func tearDown() {
        window = nil
        super.tearDown()
    }
    
    // MARK: - Private Methods
    
    private func setupACSearchViewController() {
        let bundle = Bundle.main
        let storyboard = UIStoryboard(name: kControllerStoryboard, bundle: bundle)
        
        guard let searchController = storyboard.instantiateViewController(withIdentifier:
            kControllerIdentifier) as? ACSearchViewController else {
                XCTFail("Failed - Unable to initialize controller")
                
                return
        }
        controller = searchController
    }
    
    private func loadView() {
        guard let searchController = controller, let currentWindow = window else {
            XCTFail("Failed - Unable to initialize elements")
            
            return
        }
        currentWindow.addSubview(searchController.view)
    }
    
    // MARK: - Tests
    
    func testConstantsValues() {
        loadView()
        
        guard let searchController = controller else {
            XCTFail("Failed - Unable to initialize controller")
            
            return
        }
        
        XCTAssert(searchController.kElementsFirstSection == 1, "Failed - Unexpected value for kElementsFirstSection")
        XCTAssert(searchController.kEmptyCellIdentifier == "emptyCell", "Failed - Unexpected value for kEmptyCellIdentifier")
        XCTAssert(searchController.kAllElementsCellIdentifier == "allElementsCell", "Failed - Unexpected value for kAllElementsCellIdentifier")
        XCTAssert(searchController.kCellHeight == CGFloat(44.0), "Failed - Unexpected value for kCellHeight")
    }
}
