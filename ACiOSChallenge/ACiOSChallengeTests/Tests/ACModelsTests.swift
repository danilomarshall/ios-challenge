//
//  ACModelsTests.swift
//  ACModelsTests
//
//  Created by Danilo S Marshall on 23/07/18.
//  Copyright © 2018 DevTest. All rights reserved.
//

import XCTest
import SwiftyJSON
import CoreData
@testable import ACiOSChallenge

class ACModelsTests: XCTestCase {
    
    // MARK: - Constants
    
    private let kACLocationModelFileName = "aclocationmodel"
    private let kJSONFileExtension = "json"
    
    // MARK: - Properties
    
    private var context: NSManagedObjectContext?
    
    // MARK: - Overrides
    
    override func setUp() {
        super.setUp()
        
        context = NSManagedObjectContext.contextForTests()
    }
    
    // MARK: - ACLocationModel Tests
    
    func testMockACLocationModelCreation() {
        guard let context = context else { return }
        guard let dictionaryMock = ACRetrieveMockJSON.retrieveJSONFromFile(
                                            fileName: kACLocationModelFileName,
                                            fileExtension: kJSONFileExtension,
                                            bundleType: type(of: self)) else { return }
        let mockJSON = JSON(dictionaryMock)
        do {
            let mockModel = try ACLocationModel(locationJSON: mockJSON, context: context)
            
            XCTAssert(mockModel.formattedAddress == "Houston, TX, USA", "Failed - unexpected address value")
            XCTAssert(mockModel.id == "ChIJAYWNSLS4QIYROwVl894CDco", "Failed - unexpected id value")
            XCTAssert(mockModel.latitude == Double(29.7604267), "Failed - unexpected latitude value")
            XCTAssert(mockModel.longitude == Double(-95.3698028), "Failed - unexpected longitude value")
            XCTAssert(mockModel.isSaved == false, "Failed - unexpected isSaved value")
        } catch {
            XCTFail("Failed - Unexpected error")
        }
    }
    
    // MARK: - ACLocation Tests
    
    func testMockACLocationCreation() {
        guard let context = context else { return }
        guard let dictionaryMock = ACRetrieveMockJSON.retrieveJSONFromFile(
                                                fileName: kACLocationModelFileName,
                                                fileExtension: kJSONFileExtension,
                                                bundleType: type(of: self)) else { return }
        let mockJSON = JSON(dictionaryMock)
        do {
            let mockModel = try ACLocationModel(locationJSON: mockJSON, context: context)
            ACLocation.createWithLocationModel(model: mockModel, context: context)
            
            guard let coreDataModel = ACLocation.verifyACLocationInContext(context, id: mockModel.id) else {
                XCTFail("Failed - Unexpected nil value for model")
                
                return
            }
            
            guard let coreDataId = coreDataModel.id else {
                XCTFail("Failed - Unexpected nil value for id")
                
                return
            }
            
            XCTAssert(coreDataId == mockModel.id, "Failed - unexpected id value")
        } catch {
            XCTFail("Failed - Unexpected error")
        }
    }
    
    func testMockACLocationDeletion() {
        guard let context = context else { return }
        guard let dictionaryMock = ACRetrieveMockJSON.retrieveJSONFromFile(
                                                fileName: kACLocationModelFileName,
                                                fileExtension: kJSONFileExtension,
                                                bundleType: type(of: self)) else { return }
        let mockJSON = JSON(dictionaryMock)
        do {
            let mockModel = try ACLocationModel(locationJSON: mockJSON, context: context)
            ACLocation.createWithLocationModel(model: mockModel, context: context)
            
            try ACLocation.deleteWithLocationModel(model: mockModel, context: context)
            
            guard let _ = ACLocation.verifyACLocationInContext(context, id: mockModel.id) else { return }
            
            XCTFail("Failed - Unexpected value for model")
        } catch {
            XCTFail("Failed - Unexpected error")
        }
    }
    
}
