//
//  ACRetrieveMockJSON.swift
//  ACiOSChallenge
//
//  Created by Danilo S Marshall on 27/07/18.
//  Copyright © 2018 DevTest. All rights reserved.
//

import Foundation

struct ACRetrieveMockJSON {
    
    // MARK: - Constants
    
    private static let kDefaultExtension = "json"
    
    // MARK: - Static Methods
    
    /// Method responsible for retrieving a mock with JSON dictionary
    ///
    /// - Parameters:
    ///   - fileName: string with file name
    ///   - fileExtension: specific extension of file
    ///   - bundleType: AnyClass type to retrieve bundle
    /// - Returns: Optional JSON Dictionary
    static func retrieveJSONFromFile(fileName: String, fileExtension: String, bundleType: AnyClass) -> [String: AnyObject]? {
        let bundle = Bundle(for: bundleType)
        guard let pathString = bundle.path(forResource: fileName, ofType: fileExtension) else {
            return nil
        }
        guard let jsonString = try? String(contentsOfFile: pathString, encoding: String.Encoding.utf8) else {
            return nil
        }
        
        guard let jsonData = jsonString.data(using: String.Encoding.utf8) else {
            return nil
        }
        
        guard let json = try? JSONSerialization.jsonObject(with: jsonData, options: []) as? [String: AnyObject] else {
            return nil
        }
        
        return json
    }
    
    /// Method responsible for retrieving a mock with JSON dictionary array
    ///
    /// - Parameters:
    ///   - fileName: string with file name
    ///   - fileExtension: specific extension of file
    ///   - bundleType: AnyClass type to retrieve bundle
    /// - Returns: Optional JSON Dictionary Array
    static func retrieveJSONArrayFromFile(fileName: String, fileExtension: String, bundleType: AnyClass) -> [AnyObject]? {
        let bundle = Bundle(for: bundleType)
        guard let pathString = bundle.path(forResource: fileName, ofType: fileExtension) else {
            return nil
        }
        guard let jsonString = try? String(contentsOfFile: pathString, encoding: String.Encoding.utf8) else {
            return nil
        }
        
        guard let jsonData = jsonString.data(using: String.Encoding.utf8) else {
            return nil
        }
        
        guard let jsonArray = try? JSONSerialization.jsonObject(with: jsonData, options: []) as? [AnyObject] else {
            return nil
        }
        
        return jsonArray
    }
    
    /// Method responsible for retrieving a mock with Data
    ///
    /// - Parameters:
    ///   - fileName: string with file name to retrieve
    ///   - fileKey: string with file key
    ///   - bundleType: AnyClass type to retrieve bundle
    /// - Returns: Optional Data
    static func retrieveData(fileName: String, fileKey: String, bundleType: AnyClass) -> Data? {
        let bundle = Bundle(for: bundleType)
        guard let pathString = bundle.path(forResource: fileName, ofType: "json") else {
            return nil
        }
        
        guard let jsonString = try? String(contentsOfFile: pathString, encoding: String.Encoding.utf8) else {
            return nil
        }
        
        guard let jsonData = jsonString.data(using: String.Encoding.utf8) else {
            return nil
        }
        
        guard let jsonDictionary = try? JSONSerialization.jsonObject(with: jsonData, options: []) as? [String: AnyObject] else {
            return nil
        }
        
        guard let base64String = jsonDictionary?[fileKey] as? String else {
            return nil
        }
        
        let pdfData = Data(base64Encoded: base64String)
        
        return pdfData
    }
    
    /// Method responsible for retrieving a mock with JSON dictionary
    ///
    /// - Parameters:
    ///   - dictionaryIds: [String: String] dictionary with IDs
    ///   - searchId: specific ID to retrieve
    ///   - bundleType: AnyClass type to retrieve bundle
    /// - Returns: Optional JSON Dictionary
    static func retrieveJSON(dictionaryIds: [String: String], searchId: String, bundleType: AnyClass) -> [String: AnyObject]? {
        guard let retrievedData = dictionaryIds.first(where: { (key, _) in key.contains(searchId) }) else {
            return nil
        }
        
        return retrieveJSONFromFile(fileName: retrievedData.value, fileExtension: kDefaultExtension, bundleType: bundleType)
    }
    
    /// Method responsible for retrieving a mock with JSON dictionary array
    ///
    /// - Parameters:
    ///   - dictionaryIds: [String: String] dictionary with IDs
    ///   - searchId: specific ID to retrieve
    ///   - bundleType: AnyClass type to retrieve bundle
    /// - Returns: Optional JSON Dictionary Array
    static func retrieveArrayJSON(dictionaryIds: [String: String], searchId: String, bundleType: AnyClass) -> [AnyObject]? {
        guard let retrievedData = dictionaryIds.first(where: { (key, _) in key.contains(searchId) }) else {
            return nil
        }
        
        return retrieveJSONArrayFromFile(fileName: retrievedData.value, fileExtension: kDefaultExtension, bundleType: bundleType)
    }
}
