//
//  ACSearchUISteps.swift
//  ACiOSChallengeUITests
//
//  Created by Danilo S Marshall on 27/07/18.
//  Copyright © 2018 DevTest. All rights reserved.
//

import XCTest

class ACSearchUISteps: ACBaseUISteps {
    
    // MARK: - Constants
    
    // Identifiers
    private static let kSearchViewIdentifier = "searchViewIdentifier"
    private static let kAllElementsCellIdentifier = "allElementsCellIdentifier"
    private static let kEmptyCellIdentifier = "emptyCellIdentifier"
    private static let kAddressCellIdentifier = "addressCellIdentifier"
    private static let kTableViewIdentifier = "tableViewIdentifier"
    private static let kSearchBarIdentifier = "searchBarIdentifier"
    
    // MARK - ACBaseUISteps Protocol
    
    static func waitScreen(testCase: XCTestCase) {
        let cell = XCUIApplication().otherElements[kSearchViewIdentifier]
        
        testCase.waitElementExists(element: cell)
    }
    
    // Exists
    
    static func tableExists() -> Bool {
        //let table = XCUIApplication().accessibilityElements.
        
        return true
    }
    
    static func allElementsCellExists() -> Bool {
        let cell = XCUIApplication().cells[kAllElementsCellIdentifier]
        
        return cell.exists
    }
    
    static func emptyCellExists() -> Bool {
        let cell = XCUIApplication().cells[kEmptyCellIdentifier]
        
        return cell.exists
    }
    
    static func addressCellExists() -> Bool {
        let cell = XCUIApplication().cells[kAddressCellIdentifier]
        
        return cell.exists
    }
    
    // Taps
    
    static func tapEmptyCell() {
        let cell = XCUIApplication().cells[kEmptyCellIdentifier]
        
        cell.tap()
    }
    
    static func tapAllElementsCell() {
        let cell = XCUIApplication().cells[kAllElementsCellIdentifier]
        
        cell.tap()
    }
    
    static func tapAddressCell() {
        let cell = XCUIApplication().cells[kAddressCellIdentifier]
        
        cell.tap()
    }
    
    static func tapSearchBar() {
        let bar = XCUIApplication().searchFields.firstMatch
        
        bar.tap()
    }
    
    // Fill data
    
    static func fillInSearch(text: String) {
        let bar = XCUIApplication().searchFields.firstMatch
        bar.tap()
        bar.typeText(text)
    }
}
