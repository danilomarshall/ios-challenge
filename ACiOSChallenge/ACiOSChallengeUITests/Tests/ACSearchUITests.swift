//
//  ACSearchUITests.swift
//  ACiOSChallengeUITests
//
//  Created by Danilo S Marshall on 23/07/18.
//  Copyright © 2018 DevTest. All rights reserved.
//

import XCTest

class ACSearchUITests: ACBaseUITests {
    
    // MARK: - Overrides
    
    override func setUp() {
        super.setUp()
        
        ACSearchUISteps.waitScreen(testCase: self)
    }
    
    // MARK: - Tests
    
    func testInvalidSearch() {
        let invalidText = "sdhajskdh kdfjkjfs"
//
//        // Validate screen elements
//        validateElements()
//
//        // Check if there's no change on screen elements
//        ACSearchUISteps.tapEmptyCell()
//        ACSearchUISteps.waitScreen(testCase: self)
//
//        // Fill in search
//        ACSearchUISteps.fillInSearch(text: invalidText)
//        XCUIApplication().keyboards.keys["return"].tap()
    }
    
    // MARK: - Private Methods
    
    private func validateElements() {
        guard ACSearchUISteps.emptyCellExists() else {
            XCTFail("Could not find all expected elements")
            
            return
        }
    }
}
