//
//  ACBaseUITests.swift
//  ACiOSChallengeUITests
//
//  Created by Danilo S Marshall on 27/07/18.
//  Copyright © 2018 DevTest. All rights reserved.
//

import XCTest
@testable import ACiOSChallenge

class ACBaseUITests: XCTestCase {
    
    // MARK: - Constants
    
    let app = XCUIApplication()
    
    // MARK: - Overrides
    
    override func setUp() {
        super.setUp()
        
        XCUIDevice.shared.orientation = .portrait
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = true
        
        app.launch()
    }
}
