//
//  ACXCTestCaseExtension.swift
//  ACiOSChallengeUITests
//
//  Created by Danilo S Marshall on 27/07/18.
//  Copyright © 2018 DevTest. All rights reserved.
//

import XCTest

// MARK: - XCTestCase extension

extension XCTestCase {
    
    /// Wait an elment exists to start interaction with it
    ///
    /// - Parameters:
    ///   - element: current element
    ///   - timeout: optional timeout. Default value 10 seconds
    func waitElementExists(element: XCUIElement, timeout: TimeInterval = 10) {
        let exists = NSPredicate(format: "exists == 1")
        
        expectation(for: exists, evaluatedWith: element, handler: nil)
        waitForExpectations(timeout: timeout) { (error) in
            guard let unwrappedError = error else { return }
            
            XCTFail("Fail - Element not found after until timeout: \(unwrappedError.localizedDescription)")
        }
    }
}
