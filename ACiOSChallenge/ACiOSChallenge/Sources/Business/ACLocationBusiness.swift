//
//  ACLocationBusiness.swift
//  ACiOSChallenge
//
//  Created by Danilo S Marshall on 25/07/18.
//  Copyright © 2018 DevTest. All rights reserved.
//

import Foundation
import SwiftyJSON

// MARK: - Type alias

typealias LocationsUICallback = (@escaping () throws -> [ACLocationModel]?) -> Void
typealias SaveLocationUICallback = (@escaping () throws -> Bool?) -> Void
typealias DeleteLocationUICallback = (@escaping () throws -> Bool?) -> Void

struct ACLocationBusiness {
    
    // MARK: - Constants
    
    private let kArrayKey = "results"
    
    // MARK: - Properties
    
    private var googleMapsProvider: GoogleMapsProtocol = GoogleMapsProvider()
    private let mainContext = DatabaseProvider.sharedDatabase.mainContext
    
    // MARK: - Public Methods
    
    /// Method responsible for requesting addresses given a search text
    ///
    /// - Parameters:
    ///   - searchText: string text to search on API
    ///   - completion: LocationsUICallback
    func locations(for searchText: String, completion: @escaping LocationsUICallback) {
        googleMapsProvider.locations(for: searchText) { (result) in
            do {
                // Check if data returned as expected
                guard let jsonData = try result() else {
                    completion { throw ACErrors.invalidResponse }
                    
                    return
                }
                guard let jsonArray = jsonData[self.kArrayKey].array else {
                    completion { throw ACErrors.invalidResponse }
                    
                    return
                }
                // Populate array with each location element returned
                var locationArray: [ACLocationModel] = []
                
                for json in jsonArray {
                    let newLocation = try ACLocationModel(locationJSON: json, context: self.mainContext)
                    locationArray.append(newLocation)
                }
                
                completion { locationArray }
            } catch {
                completion { throw error }
            }
        }
    }
    
    /// Method responsible for saving location on core data
    ///
    /// - Parameters:
    ///   - locationModel: location model to be used to create ACLocation data
    ///   - completion: SaveLocationUICallback
    func saveLocation(locationModel: ACLocationModel, completion: @escaping SaveLocationUICallback) {
        do {
            ACLocation.createWithLocationModel(model: locationModel, context: mainContext)
            try mainContext.save()
            
            completion { true }
        } catch {
            completion { throw error }
        }
    }
    
    /// Method responsible for deleting location on core data
    ///
    /// - Parameters:
    ///   - locationModel: location model to be used to delete ACLocation data
    ///   - completion: SaveLocationUICallback
    func deleteLocation(locationModel: ACLocationModel, completion: @escaping DeleteLocationUICallback) {
        do {
            try ACLocation.deleteWithLocationModel(model: locationModel, context: mainContext)
            try mainContext.save()
            
            completion { true }
        } catch {
            completion { throw error }
        }
    }
}
