//
//  ACMapView.swift
//  ACiOSChallenge
//
//  Created by Danilo S Marshall on 26/07/18.
//  Copyright © 2018 DevTest. All rights reserved.
//

import UIKit
import MapKit

class ACMapView: UIView {
    
    // MARK: - Properties
    
    private var locationsData: [ACLocationModel] = [] {
        didSet {
            createAnnotations()
        }
    }
    private var annotationsData: [MKAnnotation] = []
    
    // MARK: - Outlets

    @IBOutlet weak var mapView: MKMapView!
    
    // MARK: - Public methods
    
    func setupLocations(locations: [ACLocationModel]) {
        locationsData = locations
    }
    
    func getSingleLocation() -> ACLocationModel? {
        guard locationsData.count == 1, let firstLocation = locationsData.first else { return nil }
        
        return firstLocation
    }
    
    func updateSingleLocation(isSaved: Bool) {
        guard locationsData.count == 1, var firstLocation = locationsData.first else { return }
        
        firstLocation.isSaved = isSaved
        locationsData[0] = firstLocation
    }

    // MARK: - Private methods
    
    private func createAnnotations() {
        // Create annotation for each location on data array
        for location in locationsData {
            let newLocation =  CLLocationCoordinate2DMake(location.latitude as CLLocationDegrees, location.longitude as CLLocationDegrees)
            let pointAnnotation = MKPointAnnotation()
            pointAnnotation.coordinate = newLocation
            pointAnnotation.title = location.formattedAddress
            pointAnnotation.subtitle = formatCoordinateText(latitude: location.latitude, longitude: location.longitude)
            
            annotationsData.append(pointAnnotation)
        }
        mapView.addAnnotations(annotationsData)
        
        // Center map for all annotations
        mapView.showAnnotations(annotationsData, animated: true)
    }
    
    private func formatCoordinateText(latitude: Double, longitude: Double) -> String {
        return "(\(latitude) \(longitude))"
    }
}
