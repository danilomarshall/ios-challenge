//
//  ACSearchView.swift
//  ACiOSChallenge
//
//  Created by Danilo S Marshall on 24/07/18.
//  Copyright © 2018 DevTest. All rights reserved.
//

import UIKit

class ACSearchView: UIView {
    
    // MARK: - Constants

    private let kTableViewIdentifier = "tableViewIdentifier"
    private let kSearchBarIdentifier = "searchBarIdentifier"
    
    // MARK: - Outlets
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Public methods
    
    func setup() {
        tableView.isAccessibilityElement = true
        tableView.accessibilityIdentifier = kTableViewIdentifier
        tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 1))
        
        searchBar.isAccessibilityElement = true
        searchBar.accessibilityIdentifier = kSearchBarIdentifier
    }
}
