//
//  ACAddressTableViewCell.swift
//  ACiOSChallenge
//
//  Created by Danilo S Marshall on 25/07/18.
//  Copyright © 2018 DevTest. All rights reserved.
//

import UIKit

class ACAddressTableViewCell: UITableViewCell {
    
    // MARK: - Constants
    
    static let kIdentifier = "addressCell"

    // MARK: - Outlets
    
    @IBOutlet weak var addressLabel: UILabel!
    
    // MARK: - Public Methods
    
    func setup(text: String) {
        addressLabel.text = text
    }
}
