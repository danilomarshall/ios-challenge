//
//  ACLocationManager.swift
//  ACiOSChallenge
//
//  Created by Danilo S Marshall on 25/07/18.
//  Copyright © 2018 DevTest. All rights reserved.
//

import Foundation

class ACLocationManager: ACBaseManager {
    
    // MARK: - Properties
    
    private lazy var locationBusiness: ACLocationBusiness = {
        return ACLocationBusiness()
    } ()
    
    // MARK: - Public methods
    
    /// Method responsible for retrieving addresses list with a search text
    ///
    /// - Parameters:
    ///   - searchText: string text to use on search
    ///   - completion: LocationsUICallback
    func locations(for searchText: String, completion: @escaping LocationsUICallback) {
        addOperation {
            self.locationBusiness.locations(for: searchText) { (result) in
                OperationQueue.main.addOperation {
                    completion (result)
                }
            }
        }
    }
    
    /// Method responsible for saving location on core data
    ///
    /// - Parameters:
    ///   - locationModel: location model to be used to create ACLocation data
    ///   - completion: SaveLocationUICallback
    func saveLocation(locationModel: ACLocationModel, completion: @escaping SaveLocationUICallback) {
        addOperation {
            self.locationBusiness.saveLocation(locationModel: locationModel) { (result) in
                OperationQueue.main.addOperation {
                    completion (result)
                }
            }
        }
    }
    
    /// Method responsible for deleting location on core data
    ///
    /// - Parameters:
    ///   - locationModel: location model to be used to delete ACLocation data
    ///   - completion: SaveLocationUICallback
    func deleteLocation(locationModel: ACLocationModel, completion: @escaping DeleteLocationUICallback) {
        addOperation {
            self.locationBusiness.deleteLocation(locationModel: locationModel) { (result) in
                OperationQueue.main.addOperation {
                    completion (result)
                }
            }
        }
    }
}
