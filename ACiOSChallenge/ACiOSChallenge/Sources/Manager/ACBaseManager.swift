//
//  ACBaseManager.swift
//  ACiOSChallenge
//
//  Created by Danilo S Marshall on 25/07/18.
//  Copyright © 2018 DevTest. All rights reserved.
//

import Foundation

public class ACBaseManager {
    
    // MARK: - Properties
    
    var operations: OperationQueue
    
    // MARK: - Initializers
    
    /// Initializes an ACBaseManager subclass
    init() {
        operations = OperationQueue()
    }
    
    convenience init(maxConcurrentOperationCount: Int) {
        self.init()
        
        operations.maxConcurrentOperationCount = maxConcurrentOperationCount
    }
    
    // MARK: - Deinitializers
    
    deinit {
        operations.cancelAllOperations()
    }
    
    // MARK: - Public methods
    
    func addOperation(_ block: @escaping () -> Swift.Void) {
        let blockOperation = BlockOperation()
        blockOperation.addExecutionBlock { [unowned blockOperation, unowned operations] in
            guard !blockOperation.isCancelled && !operations.isSuspended else { return }
            block()
        }
        
        operations.addOperation(blockOperation)
    }
}
