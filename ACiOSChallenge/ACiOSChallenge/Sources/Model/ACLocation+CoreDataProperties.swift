//
//  ACLocation+CoreDataProperties.swift
//  ACiOSChallenge
//
//  Created by Danilo S Marshall on 26/07/18.
//  Copyright © 2018 DevTest. All rights reserved.
//
//

import Foundation
import CoreData

extension ACLocation {
    @nonobjc public class func fetchRequest() -> NSFetchRequest<ACLocation> {
        return NSFetchRequest<ACLocation>(entityName: "ACLocation")
    }

    @NSManaged public var formattedAddress: String?
    @NSManaged public var id: String?
    @NSManaged public var latitude: String?
    @NSManaged public var longitude: String?
}
