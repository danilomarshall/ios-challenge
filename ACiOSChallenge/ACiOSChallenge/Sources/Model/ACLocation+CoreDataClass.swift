//
//  ACLocation+CoreDataClass.swift
//  ACiOSChallenge
//
//  Created by Danilo S Marshall on 26/07/18.
//  Copyright © 2018 DevTest. All rights reserved.
//
//

import Foundation
import CoreData

public class ACLocation: NSManagedObject {
    
    // MARK: - Properties
    
    public var isSaved: Bool = false
    
    // MARK: - Initializer
    
    convenience init(context: NSManagedObjectContext) {
        let entityDescription: NSEntityDescription? = NSEntityDescription.entity(forEntityName: "ACLocation", in: context)
        self.init(entity: entityDescription!, insertInto: context)
    }
    
    /// Method responsible to check if id is already saved on context
    ///
    /// - Parameters:
    ///   - context: context to check for data
    ///   - id: id used to check
    /// - Returns: ACLocation model if it's already saved, nil otherwise
    class func verifyACLocationInContext(_ context: NSManagedObjectContext, id: String) -> ACLocation? {
        let request: NSFetchRequest<ACLocation> = ACLocation.fetchRequest()
        let predicate = NSPredicate.init(format: "id == %@", id)
        
        request.predicate = predicate
        
        do {
            let result: [ACLocation]? = try context.fetch(request) as [ACLocation]
            
            return result?.first
        } catch {
            return nil
        }
    }
    
    /// Method responsible for deleting an object from context
    ///
    /// - Parameters:
    ///   - context: context to check for data
    ///   - id: id used to check
    /// - Throws: throws if an error occurs when trying to fetch data
    class func deleteACLocationInContext(_ context: NSManagedObjectContext, id: String) throws {
        let request: NSFetchRequest<ACLocation> = ACLocation.fetchRequest()
        let predicate = NSPredicate.init(format: "id == %@", id)
        
        request.predicate = predicate
        
        do {
            let result = try context.fetch(request)
            for object in result {
                context.delete(object)
            }
        } catch {
            throw error
        }
    }
}
