//
//  ACLocationModel.swift
//  ACiOSChallenge
//
//  Created by Danilo S Marshall on 25/07/18.
//  Copyright © 2018 DevTest. All rights reserved.
//

import Foundation
import CoreData
import SwiftyJSON

struct ACLocationModel {
    
    // MARK: - Constants
    
    private let kIdKey = "place_id"
    private let kAddressKey = "formatted_address"
    private let kGeometryLocationKey = "geometry"
    private let kLocationKey = "location"
    private let kLatitudeKey = "lat"
    private let kLongitudeKey = "lng"
    
    // MARK: - Properties
    
    var id: String
    var formattedAddress: String
    var latitude: Double
    var longitude: Double
    var isSaved: Bool
    
    // MARK: - Initializers
    
    init(locationJSON: JSON, context: NSManagedObjectContext) throws {
        // Parsing Properties
        guard let id = locationJSON[kIdKey].string,
            let address = locationJSON[kAddressKey].string,
            let latitude = locationJSON[kGeometryLocationKey][kLocationKey][kLatitudeKey].double,
            let longitude = locationJSON[kGeometryLocationKey][kLocationKey][kLongitudeKey].double else {
                throw ACErrors.invalidResponse
        }
        
        self.id = id
        self.formattedAddress = address
        self.latitude = latitude
        self.longitude = longitude
        
        // Check if is already saved on context
        if ACLocation.verifyACLocationInContext(context, id: id) != nil {
            self.isSaved = true
        } else {
            self.isSaved = false
        }
    }
}
