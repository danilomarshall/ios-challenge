//
//  ACLocation+EntityParser.swift
//  ACiOSChallenge
//
//  Created by Danilo S Marshall on 24/07/18.
//  Copyright © 2018 DevTest. All rights reserved.
//

import Foundation
import CoreData
import SwiftyJSON

extension ACLocation {
    
    /// Method responsible for creating an ACLocation on context using an ACLocationModel
    ///
    /// - Parameters:
    ///   - model: ACLocationModel
    ///   - context: context used to add data
    class func createWithLocationModel(model: ACLocationModel, context: NSManagedObjectContext) {
        var location: ACLocation?
        
        if ACLocation.verifyACLocationInContext(context, id: model.id) != nil {
            location = ACLocation.verifyACLocationInContext(context, id: model.id)
        } else {
            location = ACLocation(context: context)
        }
        
        location?.id = model.id
        location?.formattedAddress = model.formattedAddress
        location?.latitude = "\(model.latitude)"
        location?.longitude = "\(model.longitude)"
    }
    
    /// Method responsible for deleting an ACLocation from context using an ACLocationModel
    ///
    /// - Parameters:
    ///   - model: ACLocationModel
    ///   - context: context used to delete data
    /// - Throws: throws if an error occurs trying to delete
    class func deleteWithLocationModel(model: ACLocationModel, context: NSManagedObjectContext) throws {
        do {
            if ACLocation.verifyACLocationInContext(context, id: model.id) != nil {
                try ACLocation.deleteACLocationInContext(context, id: model.id)
            }
        } catch {
            throw error
        }
    }
}
