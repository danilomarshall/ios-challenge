//
//  AppDelegate.swift
//  ACiOSChallenge
//
//  Created by Danilo S Marshall on 23/07/18.
//  Copyright © 2018 DevTest. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    // MARK: - Constants
    
    private let kDatabaseName = "LocationData"

    // MARK: - Private methods
    
    var window: UIWindow?
    
    // MARK: - Application methods

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        DatabaseProvider.sharedDatabase.setDatabaseName(databaseName: kDatabaseName)
        
        return true
    }
}
