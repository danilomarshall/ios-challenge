//
//  ACErrors.swift
//  ACiOSChallenge
//
//  Created by Danilo S Marshall on 25/07/18.
//  Copyright © 2018 DevTest. All rights reserved.
//

import Foundation

enum ACErrors: Error {
    case invalidRequest
    case invalidResponse
}
