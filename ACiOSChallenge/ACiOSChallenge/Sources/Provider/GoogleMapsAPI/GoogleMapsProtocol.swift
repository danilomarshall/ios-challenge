//
//  GoogleMapsProtocol.swift
//  ACiOSChallenge
//
//  Created by Danilo S Marshall on 25/07/18.
//  Copyright © 2018 DevTest. All rights reserved.
//

import Foundation
import SwiftyJSON

// MARK: - Type alias

typealias LocationsCallback = (() throws -> JSON?) -> Void

protocol GoogleMapsProtocol {
    
    /// Method responsible for requesting addresses given a search text
    ///
    /// - Parameters:
    ///   - searchText: string text to search on API
    ///   - completion: LocationsCallback
    func locations(for searchText: String, completion: @escaping LocationsCallback)
}
