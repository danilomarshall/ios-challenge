//
//  GoogleMapsApiProvider.swift
//  ACiOSChallenge
//
//  Created by Danilo S Marshall on 25/07/18.
//  Copyright © 2018 DevTest. All rights reserved.
//

import Foundation
import Alamofire

class GoogleMapsApiProvider {
    
    // MARK: - Constants
    
    private static let kBaseURL: String = "http://maps.googleapis.com/maps/api/geocode/json"
    
    // MARK: - Private properties
    
    private static let sharedURLSession: SessionManager = Alamofire.SessionManager.default
    
    // MARK: - Public properties
    
    static var shared: NetworkingProvider {
        return NetworkingProvider(session: sharedURLSession, baseURL: kBaseURL)
    }
}
