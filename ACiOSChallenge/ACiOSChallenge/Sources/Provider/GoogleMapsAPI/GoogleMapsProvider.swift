//
//  GoogleMapsProvider.swift
//  ACiOSChallenge
//
//  Created by Danilo S Marshall on 25/07/18.
//  Copyright © 2018 DevTest. All rights reserved.
//

import Foundation
import Alamofire

class GoogleMapsProvider: GoogleMapsProtocol {
    
    // MARK: - Constants
    
    // Endpoints
    private let kEndpointSeparator = "?"
    private let kSearchAddressEndpoint = "address="
    
    // MARK: - Public methods
    
    /// Method responsible for requesting addresses given a search text
    ///
    /// - Parameters:
    ///   - searchText: string text to search on API
    ///   - completion: LocationsCallback
    func locations(for searchText: String, completion: @escaping LocationsCallback) {
        guard !searchText.isEmpty else {
            completion { throw ACErrors.invalidRequest }
            
            return
        }
        // Fix data for url encode characters
        guard let updatedText = searchText.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed) else {
            completion { throw ACErrors.invalidRequest }
            
            return
        }
        
        let requestURL = kEndpointSeparator + kSearchAddressEndpoint + updatedText
        GoogleMapsApiProvider.shared.request(urlPath: requestURL, type: .get, parameter: [:]) { (result) in
            completion { return try result() }
        }
    }
}
