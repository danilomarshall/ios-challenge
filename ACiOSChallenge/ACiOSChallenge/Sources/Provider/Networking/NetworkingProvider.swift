//
//  NetworkingProvider.swift
//  ACiOSChallenge
//
//  Created by Danilo S Marshall on 25/07/18.
//  Copyright © 2018 DevTest. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

// MARK: - Typealias

public typealias NetworkingResponseCompletion = (() throws -> JSON?) -> Void

// MARK: - NetworkingProvider

struct NetworkingProvider {
    
    // MARK: - Constants
    
    private let session: SessionManager
    private let baseURL: String
    
    // MARK: - Initializer
    
    init(session: SessionManager, baseURL: String) {
        self.session = session
        self.baseURL = baseURL
    }
    
    // MARK: - Public Methods
    
    func getNetworkSession() -> SessionManager {
        return session
    }
    
    func request(urlPath: String, type: HTTPMethod, parameter: Parameters, completion: @escaping NetworkingResponseCompletion) {
        let fullPath = baseURL + urlPath
        Alamofire.request(fullPath, method: type, parameters: parameter).validate().responseJSON { response in
            switch response.result {
            case .success(let responseJSON):
                completion { JSON(responseJSON) }
            case .failure(let error):
                print(error)
                completion { throw error }
            }
        }
    }
}
