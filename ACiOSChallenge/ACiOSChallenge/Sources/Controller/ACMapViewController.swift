//
//  ACMapViewController.swift
//  ACiOSChallenge
//
//  Created by Danilo S Marshall on 26/07/18.
//  Copyright © 2018 DevTest. All rights reserved.
//

import UIKit
import MapKit

class ACMapViewController: UIViewController {
    
    // MARK: - Constants
    
    private let kBarButtonIdentifier = "barButtonIdentifier"
    private let kAllResultsTitle = "All Results"
    private let kSaveTitle = "Save"
    private let kDeleteTitle = "Delete"
    
    // MARK: - Outlets
    
    @IBOutlet weak var navigationTitle: UINavigationItem!
    @IBOutlet weak var navigationBarButton: UIBarButtonItem!
    
    // MARK: - Properties
    
    internal var mainView: ACMapView {
        guard let view = self.view as? ACMapView else {
            fatalError("ACMapView cast failed")
        }
        
        return view
    }
    private let locationManager = ACLocationManager()
    private var isSaving = false
    
    // MARK: - Actions
    
    @IBAction func barButtonTapped(_ sender: Any) {
        guard let singleLocation = mainView.getSingleLocation() else { return }
        
        if isSaving {
            locationManager.saveLocation(locationModel: singleLocation) { [weak self] (result) in
                guard let weakSelf = self else { return }
                
                do {
                    guard let didSucceed = try result() else {
                        print("Failed to save location")
                        
                        return
                    }
                    
                    // If saved, change button to `delete` option
                    if didSucceed {
                        weakSelf.mainView.updateSingleLocation(isSaved: true)
                        weakSelf.isSaving = false
                        weakSelf.navigationBarButton.title = weakSelf.kDeleteTitle
                    }
                } catch {
                    print(error)
                }
            }
        } else {
            let alertController = UIAlertController(title: "Alert", message: "Confirm deletion of this location?", preferredStyle: .alert)
            let deleteAction = UIAlertAction(title: "Delete", style: .default) { (_) in
                self.locationManager.deleteLocation(locationModel: singleLocation) { [weak self] (result) in
                    guard let weakSelf = self else { return }
                    
                    do {
                        guard let didSucceed = try result() else {
                            print("Failed to save location")
                            
                            return
                        }
                        
                        // If deleted, change button to `save` option
                        if didSucceed {
                            weakSelf.mainView.updateSingleLocation(isSaved: false)
                            weakSelf.isSaving = true
                            weakSelf.navigationBarButton.title = weakSelf.kSaveTitle
                        }
                    } catch {
                        print(error)
                    }
                }
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
            
            alertController.addAction(deleteAction)
            alertController.addAction(cancelAction)
            
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    // MARK: - Overrides
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mainView.mapView.delegate = self
        navigationBarButton.accessibilityIdentifier = kBarButtonIdentifier
    }
    
    // MARK: - Public methods
    
    func setData(locations: [ACLocationModel]) {
        mainView.setupLocations(locations: locations)
        
        // Check if it's a saved location to change navigation button
        guard locations.count == 1, let singleLocation = locations.first else {
            navigationTitle.title = kAllResultsTitle
            navigationBarButton.isEnabled = false
            navigationBarButton.tintColor = .clear
            
            return
        }
        navigationTitle.title = ""
        navigationBarButton.isEnabled = true
        navigationBarButton.tintColor = UIColor(red: 0/255, green: 122/255, blue: 255/255, alpha: 1.0)
        
        if singleLocation.isSaved {
            isSaving = false
            navigationBarButton.title = kDeleteTitle
        } else {
            isSaving = true
            navigationBarButton.title = kSaveTitle
        }
    }
}

// MARK: - UINavigationController delegate

extension ACMapViewController: UINavigationControllerDelegate {
    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
        guard let searchVC = viewController as? ACSearchViewController else { return }
        guard let singleLocation = mainView.getSingleLocation() else { return }
        
        searchVC.updateModel(newModel: singleLocation)
    }
}

// MARK: - MapView Delegate

extension ACMapViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard let annotation = annotation as? MKPointAnnotation else { return nil }
        let identifier = "marker"
        var view: MKPinAnnotationView
        if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
            as? MKPinAnnotationView {
            dequeuedView.annotation = annotation
            view = dequeuedView
        } else {
            view = MKPinAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            view.canShowCallout = true
            view.calloutOffset = CGPoint(x: -5, y: 5)
        }
        return view
    }
}
