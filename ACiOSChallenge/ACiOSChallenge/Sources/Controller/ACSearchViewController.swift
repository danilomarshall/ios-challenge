//
//  ACSearchViewController.swift
//  ACiOSChallenge
//
//  Created by Danilo S Marshall on 24/07/18.
//  Copyright © 2018 DevTest. All rights reserved.
//

import UIKit

enum DataToPresent {
    case single(Int)
    case all
}

class ACSearchViewController: UIViewController {
    
    // MARK: - Constants
    
    internal let kElementsFirstSection = 1
    internal let kEmptyCellIdentifier = "emptyCell"
    internal let kAllElementsCellIdentifier = "allElementsCell"
    internal let kCellHeight: CGFloat = 44.0
    private let kDetailSegueIdentifier = "mapDetail"
    
    // MARK: - Properties
    
    internal var locationsData: [ACLocationModel] = [] {
        didSet {
            mainView.tableView.reloadData()
        }
    }
    internal var mainView: ACSearchView {
        guard let view = self.view as? ACSearchView else {
            fatalError("ACSearchView cast failed")
        }
        
        return view
    }
    private let locationManager = ACLocationManager()
    internal var currentSearch = ""
    private var detailIndex: Int?

    // MARK: - Overrides
    
    override func viewDidLoad() {
        super.viewDidLoad()

        mainView.tableView.dataSource = self
        mainView.tableView.delegate = self
        mainView.setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        guard let navigationController = navigationController else { return }
        navigationController.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        guard let navigationController = navigationController else { return }
        navigationController.setNavigationBarHidden(false, animated: false)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let locationsToSend = sender as? [ACLocationModel] else { return }
        guard let mapViewController = segue.destination as? ACMapViewController else { return }
        
        mapViewController.setData(locations: locationsToSend)
    }
    
    // MARK: - Public Methods
    
    func updateModel(newModel: ACLocationModel) {
        guard let index = detailIndex else { return }
        
        locationsData[index] = newModel
    }
    
    // MARK: - Private Methods
    
    internal func updateData(textToSearch: String) {
        locationManager.locations(for: textToSearch) { [weak self] (result) in
            guard let weakSelf = self else { return }
            
            do {
                guard let dataArray = try result() else {
                    weakSelf.locationsData = []
                    return
                }
                weakSelf.locationsData = dataArray
            } catch {
                weakSelf.locationsData = []
            }
        }
    }
    
    internal func presentMapDetail(data: DataToPresent) {
        var locations: [ACLocationModel] = []
        switch data {
        case .all:
            locations = locationsData
            detailIndex = nil
        case .single(let position):
            locations.append(locationsData[position])
            detailIndex = position
        }
        
        performSegue(withIdentifier: kDetailSegueIdentifier, sender: locations)
    }
}

// MARK: - UISearchBarDelegate

extension ACSearchViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let searchText = searchBar.text, currentSearch != searchText else { return }
        
        currentSearch = searchText
        updateData(textToSearch: searchText)
    }
}

// MARK: - UITableViewDataSource & UITableViewDelegate

extension ACSearchViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        guard !locationsData.isEmpty else { return 1 }
        guard locationsData.count > 1 else { return 1 }
        
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard section == 1 else { return 1 }
        
        return locationsData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // Check each scenario before dequeuing cell
        guard !locationsData.isEmpty else {
            let cell = tableView.dequeueReusableCell(withIdentifier: kEmptyCellIdentifier, for: indexPath)
            
            return cell
        }
        guard locationsData.count > 1 else {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: ACAddressTableViewCell.kIdentifier, for: indexPath) as? ACAddressTableViewCell,
                  let firstData = locationsData.first else {
                return UITableViewCell()
            }
            cell.setup(text: firstData.formattedAddress)
            
            return cell
        }
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: kAllElementsCellIdentifier, for: indexPath)
            
            return cell
        } else {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: ACAddressTableViewCell.kIdentifier, for: indexPath) as? ACAddressTableViewCell else {
                    return UITableViewCell()
            }
            let data = locationsData[indexPath.row]
            cell.setup(text: data.formattedAddress)
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        // Adjust heights for section headers on each scenario
        guard !locationsData.isEmpty, locationsData.count > 1 else {
            let height = kCellHeight * 1.5
            return UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: height))
        }
        if section == 0 {
            return UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: kCellHeight))
        } else {
            let height = kCellHeight * 0.5
            return UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: height))
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return kCellHeight
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard !locationsData.isEmpty else { return }
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        guard locationsData.count > 1 else {
            presentMapDetail(data: .all)
            
            return
        }
        
        if indexPath.section == 0 {
            presentMapDetail(data: .all)
        } else {
            presentMapDetail(data: .single(indexPath.row))
        }
    }
}
